---
title : About
date : "2020-04-18"
---


### About this site
This site was created using [R Blogdown package](https://github.com/rstudio/blogdown) and [Hugo](https://gohugo.io/) with the theme [Hugo-Material](https://github.com/Xzya/hugo-material-blog)



### Contact
* mail : newraist@pm.me
* gitlab : https://gitlab.com/newraist
