---
title: A propos
date : "2020-04-18"
---

#### A propos du site
Le site a été cééé à partir du package [R Blogdown](https://github.com/rstudio/blogdown) et le générateur de site statique [Hugo](https://gohugo.io/).  
Le thème utilisé est [Hugo-Material](https://github.com/Xzya/hugo-material-blog)


#### Contact
* mail : newraist@pm.me
* gitlab : https://gitlab.com/newraist