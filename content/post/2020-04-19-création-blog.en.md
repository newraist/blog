---
title: Blog creation
author: Romain Naslo
date: '2020-04-19'
slug: creation-blog
categories:
  - Projets
tags:
  - hugo
  - blog
---




#### Blog set up with R

This blog was created using the R package [Blogdown](https://github.com/rstudio/blogdown) developped by Yihui Xie. With Rstudio IDE and the package you can easily generate files and folders mandatory for a static website with [Hugo](https://gohugo.io/).

Hugo's [themes](https://themes.gohugo.io/) are numerous and the [documentation](https://gohugo.io/documentation/) is well explained.

After installing the R package Blogdown, new_site() function generate the site folder with minimal example of a working site.

```r
blogdown::new_site(
	dir = "dir", 
	theme = "Xzya/hugo-material-blog", 
	sample = TRUE, 
	theme_example = TRUE, 
	empty_dirs = TRUE)
```
It is possible to previsualise live the website using the Addin "Servie site" installed with the package.



#### Site deployment
[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) is the hosting service I have chosen for its simplicity.

After configuring the project (our the group projects), you can add all files and template.
To generate automatically the pages of the static website at every commit, you need a [*.gitlab-ci.yml*](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) script. 

```yml
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```


#### Tips

1. you may use html in  markdown pages, but in order to be correctly generate, you need to add the following lines in *config.toml*
```toml
[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      unsafe = true
```
2. If you write *[ci skip]* in your commit, gitlab will not generate again the static website. It could be usefull if you do multiple commit but only want to generate the website at the end (for example, when a post is finished)


#### What's next

I think i will make my own theme one day. But it will need a little more work and research in the documentation.
