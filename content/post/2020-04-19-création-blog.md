---
title: Création d'un blog
author: Romain Naslo
date: '2020-04-19'
slug: creation-blog
categories:
  - Projets
tags:
  - hugo
  - blog
---




#### Création d'un blog avec R
Ce blog a été créé à à partir du package [R Blogdown](https://github.com/rstudio/blogdown) créé par Yihui Xie. Couplé à l'IDE RStudio, il permet de générer facilement les dossiers et documents nécessaires pour construire un site statique avec [Hugo](https://gohugo.io/).

Les [thèmes](https://themes.gohugo.io/) nombreux proposés par Hugo ainsi que la [documentation](https://gohugo.io/documentation/) a disposition permettent d'arriver rapidement à un résultat satisfaisant.

Dans Rstudio, après avoir installé le package R Blogdown, la fonction new_site() permet de générer un site avec un exemple minimal. 

```r
blogdown::new_site(
	dir = "dir", 
	theme = "Xzya/hugo-material-blog", 
	sample = TRUE, 
	theme_example = TRUE, 
	empty_dirs = TRUE)
```
Une prévisualisation en temps réel est possible grâce à l'Addin "Serve site" installé avec le package.


#### Déploiement du site
L'hébergement du blog se fait grâce aux pages [Gitlab](https://docs.gitlab.com/ee/user/project/pages/) qui proposent facilement de pouvoir déployer le site.

Afin que chaque commit sur le projet Gitlab permettent la mise à jour du site, il faut que Hugo regénère les pages du site.
Dans le documentation du package blogdown, un [script *.gitlab-ci.yml*](https://bookdown.org/yihui/blogdown/gitlab-pages.html) est proposé.  Ce script a l'avantage d'utiliser R et donc de pouvoir compiler directement des fichier *.Rmd* ou  *.Rmarkdown* en plus des *.md* classiques.

Dans le cas de ce site, je ne compte pas forcément utiliser des Rmd, donc je suis parti sur un le script [script *.gitlab-ci.yml*](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) proposé par Hugo qui permet la génération du site à partir des fichier *.md*

```yml
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```


#### Trucs et astuces

1. Les fichiers markdown autorisent l'utilisation du html au sein des pages. Afin que celui-ci soit bien utiliser lors de la génération, il est important d'ajouter les lignes suivantes dans le fichier *config.toml*
```toml
[markup]
  [markup.goldmark]
    [markup.goldmark.renderer]
      unsafe = true
```
2. Si l'on souhaite que Gitlab ne génère pas automatiquement le site lors d'un commit, il est possible d'ajouter le commenter [ci skip] dans le commit afin d'ignorer le pipeline.
Cela peut être utile si les modifications sont mineures, ou si l'on souhaite ne regénérer le site qu'une fois toutes les modifications effectuées tout en réalisant plusieurs commit (par exemple lors de la rédaction d'un article et sa publication une fois fini).
Cela permet aussi d'économiser du temps de pipeline, ce temps étant limiter en fonction de l'abonnement choisi dans Gitlab (mais les 2000 minutes / mois disponibles sur l'offre gratuite sont largement suffisante pour un usage de blogging simple). 



#### Perspectives

Pour la suite, un autre projet se concentrera sur la création d'un thème personnalisé répondant mieux à mes critères. Mais ça va nécessiter de se plonger plus en profondeur sur la documentation Hugo.